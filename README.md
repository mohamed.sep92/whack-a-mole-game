## Assignment

Create an adapted analogue of the game [Whack a mole](./moles.png).

#### Technical requirements:
- Create a 10*10 field using the ```<table>``` element.
- The essence of the game: any unlit cell in the table turns blue for a short time. The user must click on the painted cell within the allotted time. If the user managed to do this, it turns green, the user gets 1 point. If you don't have time, it turns red, the computer gets 1 point.
- The game continues until half of the cells on the field are painted green or red. As soon as this happens, the player (human or computer) with more cells on the field wins.
- The game must have three levels of difficulty, which are selected before the start of the game:
   - Easy – a new cell is highlighted every 1.5 seconds;
   - Medium - a new cell is highlighted once per second;
   - Heavy - a new cell is highlighted every half second.
- After the end of the game, display a message on the screen about who won.
- After finishing the game, it should be possible to change the difficulty level and start a new game.
- Use OOP functionality when writing a program.
- If desired, instead of coloring the cells with color, you can insert pictures there.

## Author
This task was created by DANit student as part of the completion of the "Advanced JS" course [Mohamed](https://gitlab.com/mohamed.sep92/whack-a-mole-game/-/tree/master/).

## Gitlab Website

[visit our gitlab page](https://mohamed.sep92.gitlab.io/whack-a-mole-game/)
